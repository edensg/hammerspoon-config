# Hammerspoon config files

These are my own [Hammerspoon](http://www.hammerspoon.org/) config files. I've got a URL handler and a grid system for window management. Other than that, it's pretty minimal.

Feel free to do whatever you want with this (it's CC0).